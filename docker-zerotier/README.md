### OLD approach

You can also run zerotier on the vmware host (in a docker container with --net=host or bare metal). 
In this approach the vmnet8 and zt0 interfaces are bridged with bridge 'vmnet-zt'

The bridge then needs both the ip address and the route set. Problem is that /usr/bin/vmware-networks 
will then be confused, as it assumes routes and subnet attached to vmnet8. As Vagrant regularly restarts
vmnet8, this doesn't work. 


A hack is to do some magic with the vmware-networks pre- and post-hooks to make sure things don't break. 
        * mv /usr/bin/vmware-networks /usr/bin/vmware-networks-orig
        * mv ./vmware-networks* /usr/bin/

But this becomes too fragile, so this approach is now abandoned. Leave it here for reference. 



