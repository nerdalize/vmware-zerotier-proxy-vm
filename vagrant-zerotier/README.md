# Bridging with ZeroTier

This repo is an example on how you can configure ZeroTier in bridge mode. It contains 2 components: 

* Ansible script to set up your machine as L2 bridge
* Vagrant script to create a VM for testing (but this may likely not apply to your usecase)

## VMWare 

In our situation, we use VMWare Workstation 11 to create VM's in a hostonly network on interface vmnet8 (NATed), and we want those machines to be accessible from laptops. If you do not use VMWare, forget about this section. If you do use VMWare but your goals is not to setup connectivity to the VMWare hostonly bridge, you will probably want to configure eth0/eth1 the other way around. 

You may use a bare metal server, laptop, VirtualBox VM, or whatever. We use VMWare because we want to connect the other VMWare machines, and bridging vmnet8 on the host with linux bridges confuses /usr/bin/vmware-networks to no end, as it assumes that it's subnet and routes on no other devices than vmnet8. 

You create a vmnet9 interface with the UI, or as follows: 

```
answer VNET_9_DHCP yes
answer VNET_9_DHCP_CFG_HASH AAA00E99C03363452810B8A731F2854DEADBEEFE
answer VNET_9_HOSTONLY_NETMASK 255.255.255.0
answer VNET_9_HOSTONLY_SUBNET 172.16.109.0
answer VNET_9_NAT yes
answer VNET_9_VIRTUAL_ADAPTER yes
```

Also you need to copy /etc/vmware/vmnet8 to /etc/vmware/vmnet9, and modify the files: 

* nat.mac (make it unique)
* nat/nat.conf (change device and subnet)
* dhcpd/dhcpd.conf (change device and subnet) 

Run `vmware-networks --start` and check if everything starts fine. 

Now you run `vagrant up`. It starts the Ansible deployment immediately. Note that this assumes you configured the zerotier network-id in `variables.yml`


# Ansible

Modify your inventory file to your liking. Edit `variables.yml` to contain your zerotier network id. 

Start deployment: `ansible-playbook -i inventory ansible-zerotier.yml` . 

This sets up bridging as in `roles/zerotier-bridge/templates/interfaces.j2`; you only have to click 'allow' and 'bridge mode' in the my.zerotier.com portal. 

# Allowing the bridge in my.zerotier.com

After starting the VM, you must allow the zerotier client and enable bridge-mode. 
You laptops should be configured in normal mode. 
You must configure the zerotier-portal and vmware dhcpd to give ip addresses in the *same* subnet, but *non overlapping* ip range. The bridge is not configured to receive an ip address (just L2). The laptops (non-bridge mode ZT) will receive an address from the portal (but they do not actually use dhcp). 



